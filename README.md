This is The Widget Company--we create widgets.

This repository is a modified version of the project built for the free [_Redux manufacturing_ course][manu], created by [Bytesized][bytesized]. It has been updated to use [TypeScript][typescript]. Blog post coming soon...

**Bytesized helps software teams go from unfamiliarity to fluency, through world-class technical training.**

[manu]: https://bytesized.xyz/redux-manufacturing/
[bytesized]: https://bytesized.xyz
[typescript]: https://www.typescriptlang.org/

