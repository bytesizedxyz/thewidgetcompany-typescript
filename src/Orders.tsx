import * as React from "react";
import { Component } from "react";

import { IOrder } from "./types";

interface IProps {
  generateOrder: () => void;
  orders: IOrder[];
}

class Orders extends Component<IProps, {}> {
  public render() {
    const { generateOrder, orders } = this.props;
    return (
      <div>
        <h2>Orders</h2>
        <h3>{orders.length} orders</h3>
        <ul>
          {orders.map((order) => <li key={order.created.toString()}>Order for {order.widgets} widgets</li>)}
        </ul>
        <button onClick={generateOrder}>Generate order</button>
      </div>
    );
  }
}

export default Orders;
