import {
  ADD_WIDGET,
  DOWELS_NEEDED,
  GENERATE_ORDER,
  ORDER_MATERIALS,
  PACKAGE_ORDER,
  PRESENT_ERROR,
  SCREWS_NEEDED,
  SHIP_ORDER,
  WHEELS_NEEDED,
} from "./constants";

import { Dispatch } from "redux";

import {
  IAction,
  IApplicationState,
  IOrder,
  IThunkedAction,
 } from "./types";

export function attemptWidgetCreation(): IThunkedAction {
  return (dispatch: Dispatch<IApplicationState>, getState: () => IApplicationState) => {
    const { materials } = getState();
    const { dowel, screw, wheel } = materials;

    if (dowel.count >= DOWELS_NEEDED && screw.count >= SCREWS_NEEDED && wheel.count >= WHEELS_NEEDED) {
      dispatch(addWidget());
    } else {
      dispatch(presentError("Not enough materials to create a widget"));
    }
  };
}

export function addWidget(): IAction {
  return {
    type: ADD_WIDGET,
  };
}

export function generateOrder(): IAction {
  return {
    type: GENERATE_ORDER,
  };
}

export function orderMaterials(): IAction {
  return {
    type: ORDER_MATERIALS,
  };
}

export function checkOrderForPackaging(): IThunkedAction {
  return (dispatch: Dispatch<IApplicationState>, getState: () => IApplicationState) => {
    const { orders, widgets } = getState();
    const order = orders[0];
    if (order.widgets <= widgets.length) {
      dispatch(packageOrder(order));
    } else {
      dispatch(presentError("Not enough widgets to fill order!"));
    }
  };
}

export function packageOrder(order: IOrder): IAction {
  return {
    order,
    type: PACKAGE_ORDER,
  };
}

export function shipOrder(): IAction {
  return {
    type: SHIP_ORDER,
  };
}

export function presentError(message: string): IAction {
  return {
    message,
    type: PRESENT_ERROR,
  };
}

export default {
  attemptWidgetCreation,
  checkOrderForPackaging,
  generateOrder,
  orderMaterials,
  shipOrder,
};
