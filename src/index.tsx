import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";

import App from "./App";
import appState from "./reducers";

import "./index.css";

const store = createStore(
  appState,
  applyMiddleware(thunk),
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root"),
);
