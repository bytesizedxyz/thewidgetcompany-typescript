import * as React from "react";
import { Component } from "react";

import { IWidget } from "./types";

interface IProps { widgets: IWidget[]; }

class Inventory extends Component<IProps, {}> {
  public render() {
    return (
      <div>
        <h2>Inventory</h2>
        <h4>{this.props.widgets.length} widgets in inventory</h4>
      </div>
    );
  }
}

export default Inventory;
