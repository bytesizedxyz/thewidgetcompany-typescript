import * as React from "react";
import { Component } from "react";

interface IProps {
  addWidget: () => void;
}

class Manufacturing extends Component<IProps, {}> {
  public render() {
    return (
      <div>
        <h2>Manufacturing</h2>
        <button onClick={this.props.addWidget}>Manufacture widget</button>
      </div>
    );
  }
}

export default Manufacturing;
