import * as React from "react";
import { Component } from "react";
import "./index.css";
import "./normalize.css";

import { connect } from "react-redux";

import {
  IAction,
  IApplicationState,
  IDispatchAction,
} from "./types";

import {
  attemptWidgetCreation,
  checkOrderForPackaging,
  generateOrder,
  orderMaterials,
  shipOrder,
} from "./actions";

import Inventory from "./Inventory";
import Management from "./Management";
import Manufacturing from "./Manufacturing";
import Materials from "./Materials";
import Orders from "./Orders";
import Packaging from "./Packaging";
import QA from "./QA";
import Shipping from "./Shipping";

interface IProps extends IApplicationState {
  attemptWidgetCreation: IDispatchAction;
  checkOrderForPackaging: IDispatchAction;
  generateOrder: IDispatchAction;
  orderMaterials: IDispatchAction;
  shipOrder: IDispatchAction;
}

const Header = () => <img className="logo" src="/logo.png" />;

interface IErrorProps { message: string; }
const Error = (props: IErrorProps) => <h4>{props.message}</h4>;

class App extends Component<IProps, {}> {
  public render() {
    const {
      attemptWidgetCreation,
      checkOrderForPackaging,
      error,
      failed,
      generateOrder,
      materials,
      orderMaterials,
      orders,
      packaged,
      shipped,
      shipOrder,
      widgets,
    } = this.props;

    return (
      <div>
        <Header />
        {error ? <Error message={error} /> : null}
        <Orders generateOrder={generateOrder} orders={orders} />
        <Materials materials={materials} />
        <Manufacturing addWidget={attemptWidgetCreation} />
        <QA failed={failed} />
        <Inventory widgets={widgets} />
        <Packaging orders={orders} packaged={packaged} packageOrder={checkOrderForPackaging} />
        <Shipping packaged={packaged} shipped={shipped} shipOrder={shipOrder} />
        <Management orderMaterials={orderMaterials} />
      </div>
    );
  }
}

const mapStateToProps = (state: IApplicationState) => state;
const mapActionCreators = {
  attemptWidgetCreation,
  checkOrderForPackaging,
  generateOrder,
  orderMaterials,
  shipOrder,
};

export default connect(
  mapStateToProps,
  mapActionCreators,
)(App);
