import {
  ADD_WIDGET,
  DOWELS_NEEDED,
  GENERATE_ORDER,
  ORDER_MATERIALS,
  PACKAGE_ORDER,
  PRESENT_ERROR,
  SCREWS_NEEDED,
  SHIP_ORDER,
  WHEELS_NEEDED,
} from "./constants";

import {
  IAction,
  IApplicationState,
  IMaterial,
  IOrder,
  IWidget,
} from "./types";

const initialState: IApplicationState = {
  error: null,
  failed: 0,
  materials: {
    dowel: { count: 2 },
    screw: { count: 8 },
    wheel: { count: 3 },
  },
  orders: [],
  packaged: 0,
  shipped: 0,
  widgets: [],
};

const addWidget = (state: IApplicationState): IApplicationState => {
  const newMaterials = state.materials;
  newMaterials.dowel.count -= DOWELS_NEEDED;
  newMaterials.screw.count -= SCREWS_NEEDED;
  newMaterials.wheel.count -= WHEELS_NEEDED;

  let newState: { [name: string]: any };

  if (qaCheck()) {
    const newWidget: IWidget = { created: Date.now() };
    const newWidgetsInventory = [].concat(state.widgets, newWidget);
    newState = {
      error: null,
      materials: newMaterials,
      widgets: newWidgetsInventory,
    };
  } else {
    newState = {
      error: "A widget failed QA!",
      failed: state.failed += 1,
    };
  }

  return Object.assign({}, state, newState);
};

const qaCheck = (): boolean => {
  const check = Math.floor(Math.random() * 10);
  return check < 7;
};

const generateOrder = (state: IApplicationState): IApplicationState => {
  const newOrder: IOrder = {
    created: Date.now(),
    widgets: Math.floor(Math.random() * 10) + 1,
  };
  const newOrders = [].concat(state.orders, newOrder);
  return Object.assign({}, state, {
    orders: newOrders,
  });
};

const orderMaterials = (state: IApplicationState): IApplicationState => {
  const { materials } = state;
  const { dowel, screw, wheel } = materials;
  return Object.assign({}, state, {
    error: null,
    materials: {
      dowel: { count: dowel.count + 10 },
      screw: { count: screw.count + 10 },
      wheel: { count: wheel.count + 10 },
    },
  });
};

const packageOrder = (state: IApplicationState, order: IOrder): IApplicationState => {
  const newWidgets = state.widgets;

  for (let i = 0; i < order.widgets; i++) {
    newWidgets.shift();
  }

  let newOrders = [].concat(state.orders);
  newOrders = newOrders.filter((obj, index) => index !== state.orders.indexOf(obj));

  return Object.assign({}, state, {
    error: null,
    orders: newOrders,
    packaged: state.packaged + 1,
    widgets: newWidgets,
  });
};

const presentError = (state: IApplicationState, message: string): IApplicationState => {
  return Object.assign({}, state, {
    error: message,
  });
};

const shipOrder = (state: IApplicationState): IApplicationState => {
  return Object.assign({}, state, {
    error: null,
    packaged: state.packaged - 1,
    shipped: state.shipped + 1,
  });
};

export default function appState(state = initialState, action: IAction): IApplicationState {
  switch (action.type) {
    case ADD_WIDGET:
      return addWidget(state);
    case GENERATE_ORDER:
      return generateOrder(state);
    case ORDER_MATERIALS:
      return orderMaterials(state);
    case PACKAGE_ORDER:
      return packageOrder(state, action.order);
    case PRESENT_ERROR:
      return presentError(state, action.message);
    case SHIP_ORDER:
      return shipOrder(state);
    default:
      return state;
  }
}
