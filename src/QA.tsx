import * as React from "react";
import { Component } from "react";

interface IProps { failed: number; }

class QA extends Component<IProps, {}> {
  public render() {
    return (
      <div>
        <h2>QA</h2>
        <h4>{this.props.failed} widgets failed QA</h4>
      </div>
    );
  }
}

export default QA;
