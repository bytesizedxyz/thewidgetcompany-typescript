import * as React from "react";
import { Component } from "react";

import { IOrder } from "./types";

interface IProps {
  orders: IOrder[];
  packageOrder: () => void;
  packaged: number;
}

class Packaging extends Component<IProps, {}> {
  public render() {
    const { orders, packageOrder, packaged } = this.props;
    return (
      <div>
        <h2>Packaging</h2>
        <h4>{packaged} orders packaged</h4>
        {orders.length ? <button onClick={packageOrder}>Package order</button> : <h4>No orders to package</h4>}
      </div>
    );
  }
}

export default Packaging;
