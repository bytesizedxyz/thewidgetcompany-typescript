import * as React from "react";
import { Component } from "react";

interface IProps { orderMaterials: () => void; }

class Management extends Component<IProps, {}> {
  public render() {
    return (
      <div>
        <h2>Management</h2>
        <button onClick={this.props.orderMaterials}>Order raw materials</button>
      </div>
    );
  }
}

export default Management;
