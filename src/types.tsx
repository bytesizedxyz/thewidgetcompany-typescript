import { Dispatch } from "redux";

interface IAction {
  type: string;
  message?: string;
  order?: IOrder;
}

interface IApplicationState {
  error?: string;
  failed: number;
  materials: IMaterialSet;
  orders: IOrder[];
  packaged: number;
  shipped: number;
  widgets: IWidget[];
}

type IDispatchAction = () => void;

interface IMaterial {
  count: number;
}

interface IMaterialSet {
  dowel: IMaterial;
  screw: IMaterial;
  wheel: IMaterial;
}

interface IOrder {
  created: number;
  widgets: number;
}

type IThunkedAction = (dispatch: Dispatch<IApplicationState>, getState: () => IApplicationState) => void;

interface IWidget {
  created: number;
}

export {
  IAction,
  IApplicationState,
  IDispatchAction,
  IMaterial,
  IMaterialSet,
  IOrder,
  IThunkedAction,
  IWidget,
};
